# coding: utf-8
import livechanapi
import config
import requests
import websocket
import threading
import time
import json
import os
import pickle
import re
import sys

tripmap = {}
for l in requests.get('https://kotchan.org/js/tripflags.js').text.splitlines():
    if l.startswith('flags_image_table'):
        tripmap[l.split('"')[1]] = l.split('"')[3].split('.')[0]

api = livechanapi.LiveChanApi(config.url, 'int', config.password_livechan)

kotch_lc = {}
lc_kotch = {}
if os.path.exists('map'):
    kotch_lc, lc_kotch = pickle.load(open('map'))

def process_chat(api, data):
    print data
    time.sleep(0.1)
    if data['count'] in lc_kotch:
        return
    newdata = dict(data)
    files = {}
    if 'image' in data:
        file = data['image'].split('/')[-1]
        exp = file.split('.')[-1]
        localfile = 'tmp/file.%s' % exp
        with open(localfile, 'wb') as f:
            f.write(requests.get('https://kotchan.org/tmp/uploads/%s' % file).content)
        files = {'file': (data['image_filename'], open(localfile))}
    if 'trip' in data:
        newdata['icon'] = tripmap.get(data['trip'])
        if data['trip'] == '!!Vixfie/c7U':
            newdata['icon'] = 'anna'

    newdata['reply_to'] = ' '.join(
        str(lc_kotch[count])
        for count in map(int, filter(unicode.isdigit, [line.lstrip('>').split()[0] for line in data['body'].splitlines() if line.startswith('>>')]))
        if count in lc_kotch
    ) or ''
    newdata['body'] = re.sub('>>(\d+)', '', data['body'])
    newdata['thread'] = data['convo']

    res = requests.post('%s/post' % config.kotch_url, data=newdata, files=files, cookies={'password': config.password_livechan})
    if res.status_code == 200:
        res = res.json()['data']
        kotch_lc[res['count']] = data['count']
        lc_kotch[data['count']] = res['count']
        with open('map2', 'wb') as f:
            pickle.dump((kotch_lc, lc_kotch), f)
        os.rename('map2', 'map')
    else:
        print res.text

    # for st in re.findall(r'\[st\]([\w\d\-\.]+)\[\/st\]', data['body']):
    #     path = 'stickers/%s.png' % st
    #     if not os.path.exists(path):
    #         with open(path, 'wb') as f:
    #             f.write(requests.get('https://kotchan.org/images/stickers/%s.png' % st).content)
    #     sticker_data = dict(newdata)
    #     sticker_data.pop('body')
    #     res = requests.post('%s/post' % config.kotch_url, data=sticker_data, files={'file': open(path)}, cookies={'password': config.password_livechan})
    #     if res.status_code == 200:
    #         res = res.json()['data']
    #         kotch_lc[res['count']] = data['count']
    #         lc_kotch[data['count']] = res['count']
    #         with open('map', 'wb') as f:
    #             pickle.dump((kotch_lc, lc_kotch), f)

def on_message(ws, message):
    file = ''
    data = message
    if data['type'] == 'message':
        data = data['data']
        print(data)
        time.sleep(0.1)
        if data['count'] in kotch_lc:
            return
        data['country'] = data['location']['country']
        if data['location'].get('region'):
            data['country'] += '-'+data['location']['region']
        data['country_name'] = data['location']['country_name']
        if data['file']:
            file = data['file']['file']
            with open('tmp/'+file, 'wb') as f:
                f.write(requests.get('%s/static/uploads/%s' % (config.kotch_url, file)).content)
        if data.get('reply_to'):
            for count in data['reply_to']:
                if count in kotch_lc:
                    data['body'] = '>>%s\n%s' % (kotch_lc[count], data['body'])

        res = api.post(data['body'], data['name'], convo=data['thread'], file='tmp/'+file if file else None, country=data['country'])
        kotch_lc[data['count']] = res['id']
        lc_kotch[res['id']] = data['count']
        with open('map2', 'wb') as f:
            pickle.dump((kotch_lc, lc_kotch), f)
        os.rename('map2', 'map')
        print kotch_lc


def watch_livechan():
    last = requests.get('https://kotchan.org/data/int').json()[-1]['count']
    while True:
        try:
            for message in requests.get('https://kotchan.org/data/int').json():
                if message['count'] > last:
                    process_chat(api, message)
                    last = message['count']
        except Exception as e:
            print e


def watch_kotch():
    last = requests.get('%s/messages' % config.kotch_url).json()[-1]['data']['count']
    while True:
        try:
            messages = requests.get('%s/messages/%s' % (config.kotch_url, last)).json()
            for message in messages:
                if message['data']['count'] > last:
                    on_message(None, message)
                    last = message['data']['count']
            time.sleep(1)
        except Exception as e:
            print e
    # ws = websocket.WebSocketApp("%s://%s/ws?no_history=1"%('wss' if 'https' in config.kotch_url else 'ws', config.kotch_url.split('/')[-1]), on_message = on_message, on_error = restart, on_close = restart)
    # ws.run_forever()

def restart(*args):
    print 'Restarting...'
    print args
    time.sleep(1)
    python = sys.executable
    os.execl(python, python, *sys.argv)
    sys.exit(1)

def main():
    t = threading.Thread(target=watch_livechan, args=())
    t.daemon = True
    t.start()
    t2 = threading.Thread(target=watch_kotch, args=())
    t2.daemon = True
    t2.start()
    import time
    while True:
        time.sleep(1)



if __name__ == '__main__':
    main()
