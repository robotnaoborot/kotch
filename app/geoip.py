import pygeoip
import json
from .models import LocationSchema
from .settings import BASE_DIR

geoip_db = pygeoip.GeoIP(BASE_DIR / 'data/GeoLiteCity.dat')
with open(BASE_DIR / 'data/regioncodes.json') as f:
    region_names = json.load(f)


def get_location(remote_ip):
    schema = LocationSchema()
    location = geoip_db.record_by_addr(remote_ip) or {}
    return {
        'country': location.get('country_code', 'UN'),
        'region': location.get('region_code'),
        'latitude': location.get('latitude'),
        'longitude': location.get('longitude'),
        'country_name': location.get('country_name', 'UN'),
        'region_name': region_names.get('{}-{}'.format(location.get('country_code', 'UN'), location.get('region_code', ''))),
    }


def get_location_from_country(country, country_name):
    return {
        'country': country.split('-')[0],
        'region': country.split('-')[1] if '-' in country else None,
        'latitude': None,
        'longitude': None,
        'country_name': country_name,
        'region_name': region_names.get(country),
    }
