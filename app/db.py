import asyncio
import aiomongo

from .settings import settings


class ChatDB:
    def __init__(self, db=None):
        self.db = db

    async def connect(self, app=None):
        loop = asyncio.get_event_loop() if app is None else app.loop
        client = await aiomongo.create_client(settings.db_url, loop=loop)
        self.db = client.get_default_database()
        return self.db

    async def messages(self, limit=500):
        query = self.db.messages.find().limit(limit).sort("count", -1)
        res = await query.to_list()
        res.reverse()
        return res

    async def post(self, post):
        await self.db.messages.insert_one(post)
        await self.add_refs(post["count"], post["reply_to"])

    async def refs(self):
        return {rec['count']: rec for rec in await self.db.refs.find().to_list()}

    async def add_refs(self, count, reply_to):
        if not reply_to:
            return
        await self.db.refs.find_one_and_update(
            {"count": count}, {"$push": {"reply_to": {"$each": reply_to}}}, upsert=True
        )
        for ref in reply_to:
            await self.db.refs.find_one_and_update(
                {"count": ref}, {"$push": {"replies": count}}, upsert=True
            )


# db = ChatDB()
