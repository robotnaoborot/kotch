import hashlib
from .settings import settings, BASE_DIR
import json
import bbcode
import re
import IP2Proxy

db = IP2Proxy.IP2Proxy()
db.open(BASE_DIR / "data/IP2PROXY-LITE-PX4.BIN")

with open(BASE_DIR / 'data/default_names.json', 'r') as f:
    default_names = json.load(f)

with open(BASE_DIR / 'data/tripflags.json', 'r') as f:
    tripflags = json.load(f)


def get_ident(remote_ip):
    h = hashlib.sha256()
    h.update('{}{}'.format(settings.SALT, remote_ip).encode('UTF-8'))
    return h.hexdigest()


def get_default_name(country):
    return default_names.get(country, settings.default_name)


def parse_bbcode(body):
    def render_sticker(tag_name, value, options, parent, context):
        if re.match(r'^[\w\d\-\.]+$', value):
            return '<img src="https://kotchan.org/images/stickers/{}.png">'.format(value)
        else:
            return ''

    def render_flag(tag_name, value, options, parent, context):
        if re.match(r'^[\w\d\-\.]+$', value):
            return '<img src="https://kotchan.org/icons/countries2/{}.png">'.format(value)
        else:
            return ''
    parser = bbcode.Parser()
    parser.install_default_formatters()
    parser.add_formatter('st', render_sticker)
    parser.add_formatter('flag', render_flag)
    return parser.format(body)


def check_proxy(remote_ip):
    return db.get_proxy_type(remote_ip) in settings.block_proxies

