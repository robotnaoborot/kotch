from aiohttp import web
from aiohttp_remotes import XForwardedRelaxed, setup


from .settings import settings, BASE_DIR, THIS_DIR
from .views import index, post, websocket, bundle, favicon, messages, Subscription
from .db import ChatDB
# from .tasks import start_background_tasks

# import uvloop
# uvloop.install()


def setup_routes(app):

    app.add_routes([
        web.get('/', index),
        web.post('/post', post),
        web.get('/ws', websocket),
        web.get('/messages', messages),
        web.get('/messages/{last:\d+}', messages),
        web.get('/bundle.js', bundle),
        web.get('/favicon.ico', favicon),
        web.view('/subscription', Subscription),
    ])
    app.router.add_static('/static/', path=BASE_DIR / 'static/', name='static')


async def create_app():
    app = web.Application(client_max_size=settings.client_max_size)
    await setup(app, XForwardedRelaxed())
    app.update(
        name='chat',
        settings=settings
    )
    app.messages = []
    app.clients = set()
    app.clients_by_ident = {}
    app.cooldown = {}
    app.subs = []
    # app.on_startup.append(db.connect)
    app['db'] = ChatDB()
    await app['db'].connect()
    # app.on_startup.append(start_background_tasks)

    setup_routes(app)
    return app
