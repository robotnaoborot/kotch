from datetime import datetime
import json
from uuid import uuid1

import aiohttp
from aiohttp import web
import asyncio

from pywebpush import webpush, WebPushException

from .utils import get_ident
from .geoip import get_location, get_location_from_country
from .settings import BASE_DIR, settings
from .models import MessageSchema
from .thumbnails import make_thumbnail, get_extension


async def index(request):
    # if request.cookies.get('secret') != settings.secret:
    #     return
    return web.FileResponse(BASE_DIR / 'templates/chat.html')


async def favicon(request):
    return web.FileResponse(BASE_DIR / 'static/favicon.ico')


async def bundle(request):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get('http://localhost:8080/assets/bundle.js') as resp:
                if resp.status == 200:
                    res = await resp.text()
                    return web.Response(text=res)
    except:
        pass
    return web.FileResponse('static/js/bundle.js')


async def post(request):
    is_bot = request.cookies.get('password') == settings.password
    if not is_bot:
        if request.remote in request.app.cooldown:
            if (datetime.now()-request.app.cooldown[request.remote]).seconds < settings.cooldown_timer:
                return web.json_response({'error': {'time': 'You are posting too fast'}}, status=400)
        request.app.cooldown[request.remote] = datetime.now()

    postdata = dict(await request.post())
    file = postdata.pop('file') if 'file' in postdata else None
    if file:
        postdata['file'] = {'file': file.filename, 'filename': file.filename, 'size': 0}
    fileobj = None
    schema = MessageSchema()
    data = schema.load(postdata)
    if data.errors:
        return web.json_response({'error': data.errors}, status=400)
    data = data.data

    if file:
        fileobj = {
            'file': '{}.{}'.format(uuid1(), get_extension(file.filename)),
            'filename': file.filename,
        }
        filepath = settings.uploads_path / fileobj['file']
        with open(filepath, 'wb') as f:
            f.write(file.file.read())
        fileobj['size'] = filepath.stat().st_size
        try:
            thumb = await make_thumbnail(settings.uploads_path / fileobj['file'])
            fileobj['thumb'], fileobj['width'], fileobj['height'], fileobj['duration'], fileobj['type'] = thumb
        except Exception as e:
            return web.json_response({'error': {'file': str(e)}}, status=400)

    remote_ip = request.remote

    message = schema.load({
        'count': request.app.messages[-1]['count']+1 if request.app.messages else 1,
        'body': data.get('body'),
        'name': data.get('name'),
        'icon': data.get('icon'),
        'thread': data.get('thread'),
        'private_for': data.get('private_for'),
        'file': fileobj,
        'location': get_location(remote_ip) if not is_bot else get_location_from_country(postdata.get('country'), postdata.get('country_name')),
        'ip': remote_ip if not is_bot else postdata['identifier'],
        'reply_to': data.get('reply_to'),
         # 'reply_to_messages': [{msg['count']: msg} for msg in request.app.messages}.get(count) for count in data.get('reply_to', [])],
        'type': 'public' if not data.get('private_for') else 'private',
    })
    message_json = schema.dump_message(message.data)
    # for client in request.app.clients:
    #     if client.remote == remote_ip:
    #         if client.name != message_json.get('name', 'Unknown'):
    #             client.name = message_json.get('name', 'Unknown')
    #             await send_users(request.app)

    if not message.data.get('private_for'):
        clients = request.app.clients
    else:
        clients = [client for client in request.app.clients if client.ident in (message.data['private_for'], get_ident(message.data['ip']))]

    for client in clients:
        await client.send_json(message_json)

    if not message.data.get('private_for'):
        for sub in list(request.app.subs):
            try:
                webpush(
                    subscription_info=sub,
                    data=json.dumps(message_json['data']),
                    vapid_private_key=settings.private_key,
                    vapid_claims=settings.vapid_claims
                )
            except WebPushException as e:
                print(e)
                request.app.subs.remove(sub)

    request.app.messages.append(message.data)
    request.app.messages = request.app.messages[-settings.max_messages:]
    return web.json_response(message_json)


async def send_users(app):
    data = [dict(name=client.name, **get_location(client.remote)) for client in app.clients if client.remote != '127.0.0.1' and get_location(client.remote)]
    for client in app.clients:
        await client.send_json({'type': 'users', 'data': data})


async def websocket(request):

    ws = web.WebSocketResponse(timeout=10, receive_timeout=10)
    await ws.prepare(request)
    ws.remote = request.remote
    ws.ident = get_ident(ws.remote)
    ws.name = 'Unknown'
    request.app.clients.add(ws)
    await send_users(request.app)
    schema = MessageSchema()
    if not request.query.get('no_history'):
        for message in request.app.messages[-50:]:
            if message['type'] == 'public' or message.get('private_for') == ws.ident or get_ident(message['ip']) == ws.ident:
                await ws.send_json(schema.dump_message(message))
    while True:
        msg = await ws.receive()
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'ping':
                continue
            data = json.loads(msg.data)
            if 'name' in data:
                ws.name = data['name']
                await send_users(request.app)
        elif msg.type == web.WSMsgType.CLOSE or msg.type == web.WSMsgType.ERROR:
            break

    request.app.clients.remove(ws)
    await send_users(request.app)
    return ws


async def messages(request, last=None):
    schema = MessageSchema()
    return web.json_response([schema.dump_message(message) for message in request.app.messages if message['type'] == 'public' and (not last or message['count'] > last)])


class Subscription(web.View):

    async def get(self):
        return web.json_response({"public_key": settings.public_key})

    async def post(self):
        data = await self.request.json()
        if 'subscription_token' in data:
            self.request.app.subs.append((data['subscription_token']))
            return web.json_response(status=201)
        else:
            return web.json_response(status=400)


