import re

from marshmallow import Schema, fields, validate, pre_load, post_load, post_dump, ValidationError, validates, validates_schema, validate
from datetime import datetime
import bbcode

from .utils import get_ident, get_default_name, parse_bbcode, tripflags
from .settings import settings, BASE_DIR


class FileSchema(Schema):
    file = fields.Str()
    thumb = fields.Str(required=False, allow_none=True)
    width = fields.Integer(required=False, allow_none=True)
    height = fields.Integer(required=False, allow_none=True)
    duration = fields.Str(required=False, allow_none=True)
    filename = fields.Str()
    size = fields.Integer()
    type = fields.Str(default='image', validate=validate.OneOf(['image', 'video', 'audio']))
    # size = fields.Method('get_size', dump_only=True)

    # def get_size(self, obj):
    #     filepath = settings.uploads_path / obj['file']
    #     return filepath.stat().st_size


class LocationSchema(Schema):
    country = fields.Str()
    region = fields.Str(required=False, allow_none=True)
    country_name = fields.Str()
    region_name = fields.Str(required=False, allow_none=True)
    latitude = fields.Float(required=False, allow_none=True)
    longitude = fields.Float(required=False, allow_none=True)


class MessageSchema(Schema):
    count = fields.Integer(required=False)
    ip = fields.Str(load_only=True)
    datetime = fields.DateTime(missing=lambda: datetime.utcnow().isoformat())
    reply_to = fields.List(fields.Integer(), required=False)
    replies = fields.List(fields.Integer(), required=False)
    # reply_to_messages = fields.Nested('self', many=True, required=False)
    ident = fields.Method('get_ident', dump_only=True)
    name = fields.Str(validate=validate.Length(max=50))
    icon = fields.Str(required=False, allow_none=True)
    icon_data = fields.Method('get_icon_data', dump_only=True)
    body = fields.Str(validate=validate.Length(max=1024))
    location = fields.Nested(LocationSchema(), required=False, allow_none=True)
    file = fields.Nested(FileSchema(), required=False, allow_none=True)
    type = fields.Str(default='public', validate=validate.OneOf(['public', 'private']))
    private_for = fields.Str(required=False, allow_none=True)
    html = fields.Method('get_html', dump_only=True)
    thread = fields.Str(missing=lambda: 'General')


    def get_ident(self, obj):
        return get_ident(obj.get('ip', '127.0.0.1'))

    def dump_message(self, obj):
        dump = self.dump(obj)
        if not dump.data.get('name') and dump.data.get('location'):
            dump.data['name'] = get_default_name(dump.data['location'].get('country', ''))
        return {'type': 'message', 'data': dump.data}

    @pre_load
    def split_reply_to(self, in_data):
        if 'reply_to' in in_data:
            in_data = dict(in_data)
            if isinstance(in_data['reply_to'], str):
                in_data['reply_to'] = [count for count in in_data['reply_to'].split() if count and count.isnumeric()]
            if not in_data['reply_to']:
                del in_data['reply_to']
        return in_data

    @post_dump
    def set_default_name(self, data):
        if not data.get('name') and data.get('location'):
            data['name'] = get_default_name(data['location'].get('country', ''))
        return data

    @pre_load
    def check_icon_exists(self, in_data):
        icon = in_data.get('icon')
        if icon and icon not in tripflags:
            in_data = dict(in_data)
            del in_data['icon']
        return in_data

    def get_refs(self, data):
        return list(map(int, re.findall(r'>>(\d+)', data['body'])))

    def get_icon_data(self, obj):
        data = tripflags.get(obj.get('icon'))
        if data:
            return {'file': data[0], 'title': data[1]}

    def get_html(self, obj):
        body = obj.get('body', '')
        body = '<br>\n'.join(
            body.replace('<', '&lt;').replace('>', '&gt;').splitlines()
        )
        body = re.sub(r'&gt;&gt;(\d+)', r'<a href="#\1">&gt;&gt;\1</a>', body)
        body = parse_bbcode(body)
        return body

    # @validates('icon')
    # def validate_icon(self, icon):
    #     if icon and not (BASE_DIR / 'static/icons/tripflags' / '{}.png'.format(icon)).exists():
    #         raise ValidationError('Icon doesn\'t exist')

    @validates_schema
    def validate_not_empty(self, data):
        if not data.get('body') and not data.get('file'):
            raise ValidationError('Empty post')

