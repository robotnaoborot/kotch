import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueNativeSock from 'vue-native-websocket';
import App from './App.vue';
import moment from 'moment';
import createPersistedState from 'vuex-persistedstate';
//import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'

//Vue.use(Buefy)

Vue.use(Vuex);
Vue.use(VueRouter);

export const store = new Vuex.Store({
  state: {
    messages: [],
      messages_by_count: {},
      users: [],
      socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
    },
      settings: {
        ignored: [],
          cache: 20,
          name: ''
      }
  },
  getters: {
      messages: state => state.messages,
      messages_by_count: state => state.messages_by_count,
      isConnected: state => state.socket.isConnected,
      users: state => state.users,
      ignored: state => state.settings.ignored,
      cache: state => state.settings.cache
  },
    plugins: [createPersistedState({paths:['settings.ignored', 'settings.cache', 'settings.name']})],
  mutations: {
    addMessage: (state, payload) => {
      state.messages.push(payload);
      state.messages_by_count[payload.count] = payload;
    },
    deleteMessage: (state, payload) => {
      const index = state.messages.findIndex(t => t.count === payload);
      if(index !== -1) {
          state.messages.splice(index, 1);
          delete state.messages_by_count[payload];
      }
    },
      ignoreUser: (state, payload) => {
        if(!payload){
            state.settings.ignored = [];
        }else if (state.settings.ignored.indexOf(payload) === -1) {
            state.settings.ignored.push(payload);
          }
      },
      setCache: (state, payload) => {
        state.settings.cache = payload;
      },
      setName: (state, payload) => {
        state.settings.name = payload;
      },
    SOCKET_ONOPEN (state, event)  {
      Vue.prototype.$socket = event.currentTarget;
      state.socket.isConnected = true;
        vm.heartbeat = setInterval(function(){vm.$socket.send('ping'); return true;}, 2000);
    },
    SOCKET_ONCLOSE (state, event)  {return message;
      state.socket.isConnected = false;
        clearInterval(vm.heartbeat);
    },
    SOCKET_ONERROR (state, event)  {
      console.error(state, event)
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE (state, message)  {
      state.socket.message = message;
      switch (message.type) {
          case 'message':
              state.messages.push(message.data);
              state.messages_by_count[message.data.count] = message.data;
              window.new_messages += 1;
              break
          case 'delete':
              const index = state.messages.findIndex(t => t.count === message.data.count);
              if (index !== -1) {
                  state.messages.splice(index, 1);
                  delete state.messages_by_count[message.data.count];
              }
              break
          case 'users':
              state.users = message.data;
              break
      }
    },
    SOCKET_RECONNECT(state, count) {
      state.messages = [];
      state.messages_by_count = {};
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
  }
})

Vue.use(VueNativeSock, (location.protocol==='https:'?'wss://':'ws://')+location.host+'/ws', { store: store, format: 'json', reconnection: true });

Vue.config.productionTip = false;

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD.MM.YYYY HH:mm')
    }
});

Vue.filter('fileSize', function (num) {
  // jacked from: https://github.com/sindresorhus/pretty-bytes
  if (typeof num !== 'number' || isNaN(num)) {
    throw new TypeError('Expected a number');
  }

  var exponent;
  var unit;
  var neg = num < 0;
  var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  if (neg) {
    num = -num;
  }

  if (num < 1) {
    return (neg ? '-' : '') + num + ' B';
  }

  exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
  num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
  unit = units[exponent];

  return (neg ? '-' : '') + num + ' ' + unit;
});

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/:thread', component: App }
  ]
})

window.vm = new Vue({
    router,
    el: '#body',
    components: {App},
    render: h => h(App),
    store: store,
    methods: {
        addMessage: function(message) {
            this.$store.commit('addMessage', message)
        }
    }
});

window.new_messages = 0;
window.onblur = function() {
    window.new_messages = 0;
    window.blinker = setInterval(function(){
        if(window.new_messages) {
            if (document.title.indexOf('(') == 0) document.title = 'kotch';
            else document.title = '(' + window.new_messages + ') kotch';
        }
    }, 500)
}
window.onfocus = function() {
    clearInterval(window.blinker);
    document.title = 'kotch';
}
